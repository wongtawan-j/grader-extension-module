package message

import (
	"bytes"
	"encoding/binary"
)

type Submission struct {
	EpochNano  int64
	StudentID  string
	PublicKey  string
	SignedCode string
}

func (s *Submission) Serialize() (result []byte) {
	bint64 := make([]byte, 8)
	binary.LittleEndian.PutUint64(bint64, uint64(s.EpochNano))

	buffer := bytes.Buffer{}
	buffer.Write(bint64)
	buffer.WriteString("\uE000")
	buffer.WriteString(s.StudentID)
	buffer.WriteString("\uE000")
	buffer.WriteString(s.PublicKey)
	buffer.WriteString("\uE000")
	buffer.WriteString(s.SignedCode)
	return buffer.Bytes()
}

func (s *Submission) Deserialize(message []byte) {
	bs := bytes.Split(message, []byte("\uE000"))
	if len(bs) != 4 {
		panic("[Kafka] cannot deserialize message.")
	}
	s.EpochNano = int64(binary.LittleEndian.Uint64(bs[0]))
	s.StudentID = string(bs[1])
	s.PublicKey = string(bs[2])
	s.SignedCode = string(bs[3])
}
