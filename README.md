# Module for Grader Extension

## Disk Preparation (Production)

1. Create directories for each kafka persistence disk and change owner to 1001 in order to write to these directories.

    ```shell
    mkdir /home/kafka
    cd /home/kafka
    
    mkdir persistence
    
    chown 1001 persistence
    ```

2. Set file system type for each physical disk (check for /dev/sdx before run this command).

    ```shell
    mkfs.ext4 /dev/sdb
    
    mount /dev/sdb /home/kafka/persistence
    ```

## Kafka Cluster Setup

1. Start kafka cluster with docker-compose.yaml (use docker-compose-prod.yaml for production environment).

    ```shell
    # expose tcp 9093
    docker-compose up
    ```

2. Create kafka topic.

    ```shell
    docker exec kafka-0 kafka-topics.sh --create --topic event --partitions 2 --replication-factor 1 --zookeeper zookeeper:2181
    ```